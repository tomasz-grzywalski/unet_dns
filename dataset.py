import os
import numpy as np
import random
import yaml
import sys
from math import ceil
import librosa
import soundfile as sf
from multiprocessing import Process, JoinableQueue

from evaluation.reco import testsdr

VALTEST_LENGTH = 3100
SNR = 0.0
INITIAL_STATS_SAMPLE_SIZE = 200
SAMPLE_LENGTH_STEP_SIZE = 4

STFT_PARS = "stft_pars.yaml"
TEST_CLEAN = "test_clean.npy"
TEST_NOISE = "test_noise.npy"
VAL_CLEAN = "val_clean.npy"
VAL_NOISE = "val_noise.npy"
TRAIN_CLEAN = "train_clean.npy"
TRAIN_NOISE = "train_noise.npy"

TRAIN_SNRS = [-5.0, -4.0, -3.0, -2.0, -1.0, 0.0]

class Dataset:
    def __init__(self, data_root, train_batch_size, network, test_mode=False):
        self.train_batch_size = train_batch_size
        self.network = network
        self.K = 10.0
        self.C = 0.1

        self.stft_pars = yaml.load(open(os.path.join(data_root, STFT_PARS), 'r'))
        self.valtest_data_queue = JoinableQueue(5)
        self.valtest_proc = Process(target=self.valtest_batch_creator, args=(data_root, test_mode, self.valtest_data_queue))
        self.valtest_proc.start()

        self.train_proc = None
        if not test_mode:
            self.train_data_queue = JoinableQueue(5)
            self.train_proc = Process(target=self.train_batch_creator, args=(data_root, self.train_data_queue))
            self.train_proc.start()


    def cleanup(self):
        if self.valtest_proc:
            self.valtest_proc.terminate()
            self.valtest_proc = None
        if self.train_proc:
            self.train_proc.terminate()
            self.train_proc = None


    def generate_mask(self, S_r, S_i, Y_r, Y_i):
        M_r = (Y_r*S_r + Y_i*S_i)/(Y_r**2.0 + Y_i**2.0+1e-5)
        M_i = (Y_r*S_i - Y_i*S_r)/(Y_r**2.0 + Y_i**2.0+1e-5)

        M_r = self.K * (1.0-np.exp(-self.C*M_r))/(1.0+np.exp(-self.C*M_r))
        M_i = self.K * (1.0-np.exp(-self.C*M_i))/(1.0+np.exp(-self.C*M_i))

        return M_r, M_i


    def apply_mask(self, M_r, M_i, Y_r, Y_i):
        M_r = np.clip(M_r, -10.0 + 1e-6, 10.0 - 1e-6)
        M_i = np.clip(M_i, -10.0 + 1e-6, 10.0 - 1e-6)
        M_r = -(1.0/self.C)*np.log((self.K-M_r)/(self.K+M_r))
        M_i = -(1.0/self.C)*np.log((self.K-M_i)/(self.K+M_i))

        x_pred = (M_r*Y_r - M_i*Y_i) + 1j*(M_r*Y_i + M_i*Y_r)

        return x_pred*40.0


    def read_valtest_data(self, root, test_only):
        valtest_clean = np.load(os.path.join(root, TEST_CLEAN if test_only else VAL_CLEAN))
        valtest_noise = np.load(os.path.join(root, TEST_NOISE if test_only else VAL_NOISE))
        assert(valtest_clean.shape[0] == valtest_noise.shape[0])
        assert(valtest_clean.shape[1] == VALTEST_LENGTH * self.stft_pars['hop_length'])
        assert(np.all(np.isfinite(valtest_clean)))
        assert(np.all(np.isfinite(valtest_noise)))
        print "Loaded {} {} recordings".format(valtest_clean.shape[0], "test" if test_only else "val")
        self.print_initial_stats(valtest_clean, valtest_noise)

        return valtest_clean, valtest_noise


    def read_train_data(self, root):
        train_clean = np.load(os.path.join(root, TRAIN_CLEAN))
        train_noise = np.load(os.path.join(root, TRAIN_NOISE))
        assert(train_clean.shape[1] == VALTEST_LENGTH * self.stft_pars['hop_length'])
        assert(np.all(np.isfinite(train_clean)))
        assert(np.all(np.isfinite(train_noise)))
        print "Loaded train clean {} and noise {} recordings".format(train_clean.shape[0], train_noise.shape[0])
        self.print_initial_stats(train_clean, train_noise)

        return train_clean, train_noise


    def print_initial_stats(self, clean, noise):
        num_samples = min(clean.shape[0], noise.shape[0])
        indices = range(0, num_samples, num_samples/INITIAL_STATS_SAMPLE_SIZE)
        samples = [self.get_data(clean[idx], noise[idx], [SNR]) for idx in indices]
        
        min_lenght = 10000000000
        max_lenght = 0
        max_y = 0
        min_y = 100000
        sdr_original_y = 0.0
        nan_original_y = 0
        sdr_perfect_x = 0.0
        nan_perfect_x = 0
        for item in samples:
            min_lenght = min(min_lenght, item['input1'].shape[1])
            max_lenght = max(max_lenght, item['input1'].shape[1])

            max_y = max(max_y, item['input1'].max())
            min_y = min(min_y, item['input1'].min())

            sdr = testsdr(item['y_mag'], item['y_phase'], item['x_mag'], item['x_phase'])
            if np.isfinite(sdr):
                sdr_original_y += sdr
            else:
                nan_original_y += 1

            M_r, M_i = self.generate_mask(item['output1'], item['output2'], item['input1'], item['input2'])
            x_pred = self.apply_mask(M_r, M_i, item['input1'], item['input2'])

            x_mag = item['y_mag'].copy()
            x_mag[:256] = np.abs(x_pred)

            x_pha = item['y_phase'].copy()
            x_pha[:256] = np.angle(x_pred)

            sdr = testsdr(x_mag, x_pha, item['x_mag'], item['x_phase'])
            if np.isfinite(sdr):
                sdr_perfect_x += sdr
            else:
                nan_perfect_x += 1

        print "   min max length: {} {}".format(min_lenght, max_lenght)
        print "   min max spectrogram value: {} {}".format(min_y, max_y)
        print "   average sdr of original y: {}".format(sdr_original_y/(INITIAL_STATS_SAMPLE_SIZE - nan_original_y))
        print "   nan sdr of original y: {}".format(nan_original_y)
        print "   average sdr if net out is perfect: {}".format(sdr_perfect_x/(INITIAL_STATS_SAMPLE_SIZE - nan_perfect_x))
        print "   nan sdr if net out is perfect: {}".format(nan_perfect_x)
        print ""


    def stft_preprocess(self, wave, target_length=None):
        if wave.dtype == np.int16:
            wave = wave.astype(np.float32)/np.iinfo(np.int16).max

        if target_length is not None and wave.shape[0] != target_length:
            reps = int(ceil(float(target_length)/wave.shape[0]))
            wave = np.tile(wave, reps)[:target_length]

        return wave


    def get_data(self, x, n, snr, random_cut=None):
        x = self.stft_preprocess(x)
        n = self.stft_preprocess(n, x.shape[0])

        if random_cut is not None:
            start = random.randint(0, x.shape[0] - random_cut*self.stft_pars['hop_length'])
            x = x[start:start+random_cut*self.stft_pars['hop_length']]
            start = random.randint(0, n.shape[0] - random_cut*self.stft_pars['hop_length'])
            n = n[start:start+random_cut*self.stft_pars['hop_length']]

        snr = random.choice(snr)

        item = dict()

        rms_x = np.sqrt(np.mean(x**2.0))
        rms_n = np.sqrt(np.mean(n**2.0))

        gx = 10.0**(snr/20.0)*rms_n/rms_x if rms_x > 0.0 else 0.0
 
        x *= gx
        y = x + n

        x_complex = librosa.core.stft(x,
                hop_length = self.stft_pars['hop_length'],
                win_length = self.stft_pars['win_length'],
                window = self.stft_pars['window'],
                n_fft = self.stft_pars['n_fft'])
        correct_length = x_complex.shape[1] - x_complex.shape[1] % SAMPLE_LENGTH_STEP_SIZE
        x_complex = x_complex[:, :correct_length]

        n_complex = librosa.core.stft(n,
                hop_length = self.stft_pars['hop_length'],
                win_length = self.stft_pars['win_length'],
                window = self.stft_pars['window'],
                n_fft = self.stft_pars['n_fft'])
        n_complex = n_complex[:, :correct_length]

        y_complex = librosa.core.stft(y,
                hop_length = self.stft_pars['hop_length'],
                win_length = self.stft_pars['win_length'],
                window = self.stft_pars['window'],
                n_fft = self.stft_pars['n_fft'])
        y_complex = y_complex[:, :correct_length]

        item['x_mag'] = np.abs(x_complex)
        item['x_phase'] = np.angle(x_complex)

        item['y_mag'] = np.abs(y_complex)
        item['y_phase'] = np.angle(y_complex)

        item['n_mag'] = np.abs(n_complex)
        item['n_phase'] = np.angle(n_complex)

        item['input1'] = np.real(y_complex)[:256]/40.0
        item['input2'] = np.imag(y_complex)[:256]/40.0

        item['output1'] = np.real(x_complex)[:256]/40.0
        item['output2'] = np.imag(x_complex)[:256]/40.0

        return item


    def train_batch_creator(self, data_root, queue):
        clean, noise = self.read_train_data(data_root)

        while True:
            np.random.shuffle(clean)
            np.random.shuffle(noise)
            for inputs, targets, _ in self.batch_creator(clean,
                    noise,
                    self.train_batch_size,
                    self.network.train_length(),
                    TRAIN_SNRS,
                    False):
                queue.put((inputs.copy(), targets.copy()))
            queue.put((None, None))


    def iterate_train_minibatches(self):
        while True:
            inputs, targets = self.train_data_queue.get()
            self.train_data_queue.task_done()
            if inputs is None:
                break
            yield inputs, targets


    def valtest_batch_creator(self, data_root, test_only, queue):
        clean, noise = self.read_valtest_data(data_root, test_only)

        while True:
            for inputs, targets, sample in self.batch_creator(clean, noise, 8, VALTEST_LENGTH, [SNR], True):
                queue.put((inputs.copy(), targets.copy(), sample))
            queue.put((None, None, None))


    def iterate_valtest_minibatches(self):
        while True:
            inputs, targets, sample = self.valtest_data_queue.get()
            self.valtest_data_queue.task_done()
            if inputs is None:
                break
            yield inputs, targets, sample


    def batch_creator(self, clean, noise, batch_size, train_length, snr, get_sample):
        inputs = np.empty((batch_size, 2, self.network.num_freq(), train_length), dtype=np.float32)
        targets = np.empty((batch_size, 2, self.network.num_freq(), train_length), dtype=np.float32)
        
        sample_idx = 0
        while sample_idx + batch_size <= min(clean.shape[0], noise.shape[0]):
            inputs.fill(0.0)
            targets.fill(0.0)
            samples = list()
            for batch_idx in range(batch_size):
                sample = self.get_data(clean[sample_idx + batch_idx], noise[sample_idx + batch_idx], snr, train_length)
                assert(sample['input1'].shape[0] == self.network.num_freq())
                assert(sample['input1'].shape[1] == train_length)

                inputs[batch_idx, 0] = sample['input1']
                inputs[batch_idx, 1] = sample['input2']

                M_r, M_i = self.generate_mask(sample['output1'], sample['output2'], sample['input1'], sample['input2'])

                targets[batch_idx, 0] = M_r
                targets[batch_idx, 1] = M_i

                if get_sample:
                    samples.append((sample['x_mag'],
                                    sample['x_phase'],
                                    sample['y_mag'],
                                    sample['y_phase'],
                                    sample['n_mag'],
                                    sample['n_phase']))

            sample_idx += batch_size

            yield inputs, targets, samples
