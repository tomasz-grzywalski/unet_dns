import numpy as np
import theano
import theano.tensor as T
import lasagne

from base import NetworkBase

class Network(NetworkBase):
    lstm_grad_clip = 100

    @staticmethod
    def train_length():
        return 500

    @staticmethod
    def num_freq():
        return 256

    @staticmethod
    def learning_rate():
        return 0.002

    @staticmethod
    def RT_block(network, num_units):
        num_freqs = lasagne.layers.get_output_shape(network)[2]

        recu_net = lasagne.layers.dimshuffle(network, (0, 2, 3, 1))
        recu_net = lasagne.layers.ReshapeLayer(recu_net, (-1, [2], [3]))
        lstm1 = lasagne.layers.GRULayer(recu_net, num_units/2, grad_clipping=Network.lstm_grad_clip,
                                        backwards=False, learn_init=True)
        lstm1 = lasagne.layers.batch_norm(lstm1, axes=(0, 1))
        lstm2 = lasagne.layers.GRULayer(recu_net, num_units/2, grad_clipping=Network.lstm_grad_clip,
                                        backwards=True, learn_init=True)
        lstm2 = lasagne.layers.batch_norm(lstm2, axes=(0, 1))
        recu_net = lasagne.layers.ConcatLayer([lstm1, lstm2], axis=2)
        recu_net = lasagne.layers.ReshapeLayer(recu_net, (-1, num_freqs, [1], [2]))
        recu_net = lasagne.layers.dimshuffle(recu_net, (0, 3, 1, 2))

        network = lasagne.layers.ConcatLayer([network, recu_net], axis=1)

        return network

    @staticmethod
    def RF_block(network, num_units, num_frames):
        recu_net = lasagne.layers.dimshuffle(network, (0, 3, 2, 1))
        recu_net = lasagne.layers.ReshapeLayer(recu_net, (-1, [2], [3]))
        lstm1 = lasagne.layers.GRULayer(recu_net, num_units/2, grad_clipping=Network.lstm_grad_clip,
                                        backwards=False, learn_init=True)
        lstm1 = lasagne.layers.batch_norm(lstm1, axes=(0, 1))
        lstm2 = lasagne.layers.GRULayer(recu_net, num_units/2, grad_clipping=Network.lstm_grad_clip,
                                        backwards=True, learn_init=True)
        lstm2 = lasagne.layers.batch_norm(lstm2, axes=(0, 1))
        recu_net = lasagne.layers.ConcatLayer([lstm1, lstm2], axis=2)
        recu_net = lasagne.layers.ReshapeLayer(recu_net, (-1, num_frames, [1], [2]))
        recu_net = lasagne.layers.dimshuffle(recu_net, (0, 3, 2, 1))

        network = lasagne.layers.ConcatLayer([network, recu_net], axis=1)

        return network

    @staticmethod
    def build(in_var):
        NUM_C = 81
        NUM_R = 27

        network = lasagne.layers.InputLayer(shape=(None, 2, Network.num_freq(), None),
                                        input_var=in_var)
        _, _, _, num_frames = network.input_var.shape

        network = lasagne.layers.Conv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 3), stride=(2, 1), pad='same',
                nonlinearity=lasagne.nonlinearities.identity)

        net_lvl1 = network
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RT_block(network, NUM_R)
        network = lasagne.layers.Conv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 3), stride=(1, 1), pad='same',
                nonlinearity=lasagne.nonlinearities.identity)

        net_lvl2 = network
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RF_block(network, NUM_R, num_frames)
        network = lasagne.layers.Conv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 3), stride=(2, 1), pad='same',
                nonlinearity=lasagne.nonlinearities.identity)

        net_lvl3 = network
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RT_block(network, NUM_R)
        network = lasagne.layers.Conv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 3), stride=(1, 1), pad='same',
                nonlinearity=lasagne.nonlinearities.identity)

        net_lvl4 = network
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RF_block(network, NUM_R, num_frames)
        network = lasagne.layers.Conv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 3), stride=(1, 2), pad='same',
                nonlinearity=lasagne.nonlinearities.identity)

        net_lvl5 = network
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RT_block(network, NUM_R)
        network = lasagne.layers.Conv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 3), stride=(1, 1), pad='same',
                nonlinearity=lasagne.nonlinearities.identity)
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RF_block(network, NUM_R, num_frames/2)
        network = lasagne.layers.Conv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 3), stride=(1, 1), pad='same',
                nonlinearity=lasagne.nonlinearities.identity)

        network = lasagne.layers.ElemwiseSumLayer([network, net_lvl5])
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RT_block(network, NUM_R)
        network = lasagne.layers.TransposedConv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 6), stride=(1, 2), crop=(1, 2),
                nonlinearity=lasagne.nonlinearities.identity)

        network = lasagne.layers.ElemwiseSumLayer([network, net_lvl4])
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RF_block(network, NUM_R, num_frames)
        network = lasagne.layers.Conv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 3), stride=(1, 1), pad='same',
                nonlinearity=lasagne.nonlinearities.identity)

        network = lasagne.layers.ElemwiseSumLayer([network, net_lvl3])
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RT_block(network, NUM_R)
        network = lasagne.layers.TransposedConv2DLayer(
                network, num_filters=NUM_C, filter_size=(6, 3), stride=(2, 1), crop=(2, 1),
                nonlinearity=lasagne.nonlinearities.identity)

        network = lasagne.layers.ElemwiseSumLayer([network, net_lvl2])
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = Network.RF_block(network, NUM_R, num_frames)
        network = lasagne.layers.Conv2DLayer(
                network, num_filters=NUM_C, filter_size=(3, 3), stride=(1, 1), pad='same',
                nonlinearity=lasagne.nonlinearities.identity)

        network = lasagne.layers.ElemwiseSumLayer([network, net_lvl1])
        network = lasagne.layers.batch_norm(network)
        network = lasagne.layers.NonlinearityLayer(network, lasagne.nonlinearities.elu)

        network = lasagne.layers.TransposedConv2DLayer(
                network, num_filters=2, filter_size=(6, 3), stride=(2, 1), crop=(2, 1),
                nonlinearity=lasagne.nonlinearities.identity)

        return network

