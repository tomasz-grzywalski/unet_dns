#!/usr/bin/env python
import sys
import os
import random
import numpy as np
import soundfile as sf
import librosa
from math import ceil
from tqdm import tqdm

ROOT = '/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/'

TEST_CLEAN = "/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/preprocessed_16k/test_clean.npy"
TEST_SPEAK = "/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/preprocessed_16k/test_speak.npy"
TEST_NOISE = "/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/preprocessed_16k/test_noise.npy"
VAL_CLEAN = "/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/preprocessed_16k/val_clean.npy"
VAL_SPEAK = "/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/preprocessed_16k/val_speak.npy"
VAL_NOISE = "/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/preprocessed_16k/val_noise.npy"
TRAIN_CLEAN = "/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/preprocessed_16k/train_clean.npy"
TRAIN_SPEAK = "/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/preprocessed_16k/train_speak.npy"
TRAIN_NOISE = "/mnt/NAS_ML/grzywalski/ProcaNetData/DNS-Challenge/preprocessed_16k/train_noise.npy"

def list_waves_in_dir(directory):
    waves = list()
    for r, d, f in os.walk(directory):
        for file in f:
            if file.endswith(".wav"):
                waves.append(os.path.join(directory, file))
    return waves

def read_wave(filename, covert_to_int=False):
    wave, sr = sf.read(filename)

    if len(wave.shape) > 1:
        wave = wave.T
        wave = wave.sum(axis=0)/wave.shape[0]

    if sr != 16000:
        wave = librosa.core.resample(wave, sr, 16000)

    clipping = 0
    if covert_to_int:
        if np.absolute(wave).max() > 1.0:
            wave /= np.absolute(wave).max()
            clipping = 1
        wave *= np.iinfo(np.int16).max
        wave = wave.astype('int16')

    return wave, clipping

def get_waves(filenames, expected_length):
    waves_array = np.zeros((len(filenames), expected_length*16000), dtype=np.int16)
    too_short_cnt = 0
    too_long_cnt = 0
    clip_cnt = 0
    for idx, filename in enumerate(tqdm(filenames)):
        wave, clipping = read_wave(filename, covert_to_int=True)
        clip_cnt += clipping
        if wave.shape[0] > expected_length*16000:
            too_long_cnt += 1
            wave = wave[:expected_length*16000]
        if wave.shape[0] < expected_length*16000:
            too_short_cnt += 1
            reps = int(ceil(float(expected_length*16000)/wave.shape[0]))
            wave = np.tile(wave, reps)[:expected_length*16000]

        waves_array[idx] = wave

    return waves_array, too_short_cnt, too_long_cnt, clip_cnt

if __name__ == '__main__':
    random.seed(123)
    np.random.seed(123)

    clean_files = list_waves_in_dir(os.path.join(ROOT, 'clean'))
    clean_files.sort()
    speker_ids = [l.split('_')[-2] for l in clean_files]
    unique_speakers = list(set(speker_ids))
    random.shuffle(unique_speakers)

    test_speakers = unique_speakers[:100]
    test_clean_files = [p[0] for p in zip(clean_files, speker_ids) if p[1] in test_speakers]
    test_speakers = [p[1] for p in zip(clean_files, speker_ids) if p[1] in test_speakers]
    print "Test files: {}".format(len(test_clean_files))

    val_speakers = unique_speakers[100:200]
    val_clean_files = [p[0] for p in zip(clean_files, speker_ids) if p[1] in val_speakers]
    val_speakers = [p[1] for p in zip(clean_files, speker_ids) if p[1] in val_speakers]
    print "Val files: {}".format(len(val_clean_files))

    train_speakers = unique_speakers[200:]
    train_clean_files = [p[0] for p in zip(clean_files, speker_ids) if p[1] in train_speakers]
    train_speakers = [p[1] for p in zip(clean_files, speker_ids) if p[1] in train_speakers]
    print "Train files: {}".format(len(train_clean_files))

    noise_files = list_waves_in_dir(os.path.join(ROOT, 'noise'))
    random.shuffle(noise_files)

    test_noise_files = noise_files[:len(test_clean_files)]
    noise_files = noise_files[len(test_clean_files):]

    val_noise_files = noise_files[:len(val_clean_files)]
    train_noise_files = noise_files[len(val_clean_files):]

    print ""
    print "Test clean recordings..."
    test_clean, ts, tl, cl = get_waves(test_clean_files, 31)
    print "Out shape: {} too short: {} too long: {} clipping: {}\n".format(test_clean.shape, ts, tl, cl)
    np.save(TEST_CLEAN, test_clean)
    del test_clean
    np.save(TEST_SPEAK, np.array(test_speakers, dtype=np.uint16))

    print "Test noise recordings..."
    test_noise, ts, tl, cl = get_waves(test_noise_files, 10)
    print "Out shape: {} too short: {} too long: {} clipping: {}\n".format(test_noise.shape, ts, tl, cl)
    np.save(TEST_NOISE, test_noise)
    del test_noise

    print "Val clean recordings..."
    val_clean, ts, tl, cl = get_waves(val_clean_files, 31)
    print "Out shape: {} too short: {} too long: {} clipping: {}\n".format(val_clean.shape, ts, tl, cl)
    np.save(VAL_CLEAN, val_clean)
    del val_clean
    np.save(VAL_SPEAK, np.array(val_speakers, dtype=np.uint16))

    print "Val noise recordings..."
    val_noise, ts, tl, cl = get_waves(val_noise_files, 10)
    print "Out shape: {} too short: {} too long: {} clipping: {}\n".format(val_noise.shape, ts, tl, cl)
    np.save(VAL_NOISE, val_noise)
    del val_noise

    print "Train clean recordings..."
    train_clean, ts, tl, cl = get_waves(train_clean_files, 31)
    print "Out shape: {} too short: {} too long: {} clipping: {}\n".format(train_clean.shape, ts, tl, cl)
    np.save(TRAIN_CLEAN, train_clean)
    del train_clean
    np.save(TRAIN_SPEAK, np.array(train_speakers, dtype=np.uint16))

    print "Train noise recordings..."
    train_noise, ts, tl, cl = get_waves(train_noise_files, 10)
    print "Out shape: {} too short: {} too long: {} clipping: {}\n".format(train_noise.shape, ts, tl, cl)
    np.save(TRAIN_NOISE, train_noise)
    del train_noise
