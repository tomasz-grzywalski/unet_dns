#!/usr/bin/env python
import sys
sys.setrecursionlimit(1500)
import os
import time
import random
import numpy as np
import theano
import theano.tensor as T
import lasagne
import librosa
import yaml

from dataset import Dataset
from networks.unet_all_rc_2m import Network
from evaluation.reco import testsdr


def fold(dataset, model_filename):
    input_spec_var = T.tensor4('inputs_spec')

    net = Network.build(input_spec_var)
    base_data = np.load(model_filename, allow_pickle=True)
    base_data = base_data[base_data.keys()[0]]
    lasagne.layers.set_all_param_values(net, base_data)

    sys.stdout.flush()

    test_output = lasagne.layers.get_output(net, deterministic=True)

    val_fun = theano.function(
        [input_spec_var],
        [test_output])

    print("Starting testing...")
    test_sdr = 0.0
    test_sdr_nan = 0
    test_batches = 0
    for batch in dataset.iterate_valtest_minibatches():
        i, _, d = batch
        pred = val_fun(i)[0]

        for idx in range(pred.shape[0]):
            sdr = calc_sdr(d[idx], pred[idx], i[idx], dataset)
            if np.isfinite(sdr):
                test_sdr += sdr
                test_batches += 1
            else:
                test_sdr_nan += 1

    print("   test      sdr:  \t\t{:.6f}".format(test_sdr / test_batches))
    print("   test  nan sdr:  \t\t{}".format(test_sdr_nan))


def calc_sdr(original_data, predictions, inputs, dataset):
    x_mag, x_phase, y_mag, y_phase, _, _ = original_data

    m_real_pred = predictions[0]
    m_imag_pred = predictions[1]

    in_real = inputs[0]
    in_imag = inputs[1]

    x_pred = dataset.apply_mask(m_real_pred, m_imag_pred, in_real, in_imag)

    x_mag_pred = y_mag.copy()
    x_mag_pred[:256] = np.abs(x_pred)

    x_phase_pred = y_phase.copy()
    x_phase_pred[:256] = np.angle(x_pred)

    return testsdr(x_mag_pred, x_phase_pred, x_mag, x_phase)


if __name__ == '__main__':
    random.seed(123)
    np.random.seed(123)

    if len(sys.argv) < 2:
        print "You must provide model filename"
        exit(0)

    paths = yaml.load(open('./properties.yaml', 'r'))
    dataset = Dataset(paths['data_path'], None, Network, test_mode=True)
    fold(dataset, sys.argv[1])
    dataset.cleanup()
