#!/usr/bin/env python
import sys
sys.setrecursionlimit(1500)
import os
import time
import random
import numpy as np
import theano
import theano.tensor as T
import lasagne
import librosa
import cPickle as pickle
import yaml

from dataset import Dataset
from networks.unet_all_rc_2m import Network
from evaluation.reco import testsdr


def get_default_pars():
    pars = {'epochs' : 50,
            'batch_size' : 10,
            'learning_rate_decay' : 0.9,
            'properties_file': './properties.yaml'}
    return pars

def fold(dataset, pars, out_path):
    num_epochs = pars['epochs']
    learning_rate_decay = pars['learning_rate_decay']

    input_spec_var = T.tensor4('inputs_spec')
    target_var = T.tensor4('targets')

    net = Network.build(input_spec_var)
    print "Total number of network's parameters: {}".format(lasagne.layers.count_params(net))
    sys.stdout.flush()

    train_output = lasagne.layers.get_output(net)
    train_loss = ((train_output - target_var)**2.0).mean(axis=2, keepdims=True)
    train_loss = train_loss.mean()

    test_output = lasagne.layers.get_output(net, deterministic=True)
    test_loss = ((test_output - target_var)**2.0).mean(axis=2, keepdims=True)
    test_loss = test_loss.mean()

    all_params = lasagne.layers.get_all_params(net, trainable=True)
    learning_rate = theano.shared(lasagne.utils.floatX(Network.learning_rate()))
    updates = lasagne.updates.adam(train_loss, all_params, learning_rate=learning_rate)

    train_fun = theano.function(
        [input_spec_var, target_var],
        train_loss, updates=updates)

    val_fun = theano.function(
        [input_spec_var, target_var],
        [test_loss, test_output])

    print("Starting training...")
    sys.stdout.flush()
    best_val_sdr = 0.0
    prev_train_err = 1000000.0
    # We iterate over epochs:
    for epoch in range(num_epochs):
        # In each epoch, we do a full pass over the training data:
        train_err = prev_train_err * 2.0
        train_batches = 1
        start_time = time.time()

        backup = lasagne.layers.get_all_param_values(net)
        while train_err / train_batches > 1.1 * prev_train_err:
            if train_batches > 1:
                lasagne.layers.set_all_param_values(net, backup)

            train_err = 0
            train_batches = 0
            for batch in dataset.iterate_train_minibatches():
                i, t = batch
                train_err += train_fun(i, t)
                train_batches += 1
            if train_err / train_batches > 1.1 * prev_train_err:
                print("Epoch failed!")
                print("   training loss:  \t\t{:.6f}".format(train_err / train_batches))

        prev_train_err = train_err / train_batches
        learning_rate.set_value(lasagne.utils.floatX(learning_rate.get_value() * learning_rate_decay))
        model_data = lasagne.layers.get_all_param_values(net)
        np.savez(os.path.join(out_path, "latest.npz"), model_data)

        # And a full pass over the validation data:
        val_time = time.time()
        val_err = 0
        val_batches = 0
        val_sdr = 0.0
        val_sdr_nan = 0
        val_sdr_ok = 0
        for batch in dataset.iterate_valtest_minibatches():
            i, t, d = batch
            err, pred = val_fun(i, t)
            val_err += err
            val_batches += 1

            for idx in range(pred.shape[0]):
                sdr = calc_sdr(d[idx], pred[idx], i[idx], dataset)
                if np.isfinite(sdr):
                    val_sdr += sdr
                    val_sdr_ok += 1
                else:
                    val_sdr_nan += 1

        # Then we print the results for this epoch:
        print("Epoch {} of {} took {:.3f}s + {:.3f}s".format(
            epoch + 1, num_epochs, val_time - start_time, time.time() - val_time))
        print("   training loss:  \t\t{:.6f}".format(train_err / train_batches))
        print("   val      loss:  \t\t{:.6f}".format(val_err / val_batches))
        print("   val       sdr:  \t\t{:.6f}".format(val_sdr / val_sdr_ok))
        print("   val   nan sdr:  \t\t{}".format(val_sdr_nan))

        if val_sdr / val_sdr_ok > best_val_sdr:
            print "   new best val sdr!"
            best_val_sdr = val_sdr / val_sdr_ok

            model_data = lasagne.layers.get_all_param_values(net)
            np.savez(os.path.join(out_path, "model_weights.npz"), model_data)
           
    return

def calc_sdr(original_data, predictions, inputs, dataset):
    x_mag, x_phase, y_mag, y_phase, _, _ = original_data

    m_real_pred = predictions[0]
    m_imag_pred = predictions[1]

    in_real = inputs[0]
    in_imag = inputs[1]

    x_pred = dataset.apply_mask(m_real_pred, m_imag_pred, in_real, in_imag)

    x_mag_pred = y_mag.copy()
    x_mag_pred[:256] = np.abs(x_pred)

    x_phase_pred = y_phase.copy()
    x_phase_pred[:256] = np.angle(x_pred)

    return testsdr(x_mag_pred, x_phase_pred, x_mag, x_phase)

if __name__ == '__main__':
    random.seed(123)
    np.random.seed(123)

    if len(sys.argv) == 1:
        pars = get_default_pars()
    elif len(sys.argv) == 2:
        pars = yaml.load(open(sys.argv[1], 'r'))

    paths = yaml.load(open(pars['properties_file'], 'r'))
    batch_size = pars['batch_size']

    dataset = Dataset(paths['data_path'], batch_size, Network)
    fold(dataset, pars, paths['output_path'])
    dataset.cleanup()
